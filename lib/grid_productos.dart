import 'package:flutter/material.dart';

class GridProductos extends StatelessWidget {
  const GridProductos({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return  Scaffold(
      body:  Container(
      padding: const  EdgeInsets.all(5.0),
        height: double.infinity,
        child: Column(
        
          children: [ 
            //Text('Screen Grid Productos '),
            GridView(
              
                shrinkWrap: true,
               scrollDirection: Axis.vertical,
                gridDelegate:  const SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 2,
                  mainAxisExtent: 322.0,
                  crossAxisSpacing: 0.0
                  ),
                
                children:[ 
                  
                  Container(
                    width: 1.0,
                //color: Colors.amber,
                margin: const EdgeInsets.all(7.0),
                
                decoration:  BoxDecoration(
                   color: Colors.white,  
                   borderRadius: BorderRadius.circular(13.0),
                   boxShadow: const [
                      BoxShadow(
                          color: Colors.black38,
                          offset: Offset(0.0, 1.5),
                          blurStyle: BlurStyle.outer,
                          blurRadius: 2.5)
                    ]),
                            
                padding: const EdgeInsets.only(bottom: 10.0),
                child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                 children: [
                    ClipRRect(
                      borderRadius: const BorderRadius.only(topLeft: Radius.circular(13.0), topRight: Radius.circular(13.0)),
                      child: Container(
                          width: MediaQuery.of(context).size.width,
                          height: 100,
                          decoration: const BoxDecoration(
                            image: DecorationImage(
                              fit: BoxFit.fill,
                              image: AssetImage("assets/image/product_1.jpeg"),
                            ),
                          ),
                        ),
                    ),
                  
                  
                    // ClipRRect(
                    //   borderRadius: const BorderRadius.only(topLeft: Radius.circular(13.0), topRight: Radius.circular(13.0)),
                    //   child: Container(
                    //     width: 182.0,
                    //     color: Colors.red,
                    //     child: Image.asset('assets/image/product_1.jpeg'),
                    //   ),
                    // ),
                 
                  const SizedBox(height: 10.0),
                
                  Container(
                    padding: const EdgeInsets.only(left: 8.0),
                    
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: const [
                         Text('Almendra Cubierta \nen Chocolate -\nGozana ', 
                         style:  TextStyle( fontSize: 18.0, 
                         fontWeight: FontWeight.bold, color: Color(0xff00003D) ),
                         ),
                         SizedBox(height: 5.0),
                         Text('12gr', style: TextStyle(color: Colors.grey), 
                         ),
                         SizedBox(height: 5.0),
                         Text('Gozana', style: TextStyle(color: Colors.green, fontWeight: FontWeight.bold), ), 
                         SizedBox(height: 18.0),
                      ],
                    ),
                  ),
                  

                Row(
                  children: const [
                    SizedBox(width: 17),
                     Text('S/ 15.00', style: TextStyle(color: Colors.black, fontSize: 20.0, fontWeight: FontWeight.bold), ),
                      SizedBox(width: 10.0),
                     Text('S/ 19.00', style: TextStyle(color: Colors.grey, fontSize: 14.0, fontWeight: FontWeight.bold), ),                   
                  ],
                ), 
                
                
                  const Spacer(),
                  Center(
                    child: Container(
                     decoration: BoxDecoration(
                      color: Colors.green,
                    border: Border.all(color: Colors.grey),
                    borderRadius: BorderRadius.circular(25)

                     ),
                        width: 150.0,
                        height: 35.0,
                      
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                       
                        children: const [
                          
                           //SizedBox(),
                           CircleAvatar(
                            radius: 13.5,
                            child:  Text('—', style:  TextStyle(fontSize: 25.0)),
                            backgroundColor: Colors.white, 
                          ),
                          //SizedBox(width: 15,),
                            Text('1', style:  TextStyle(fontSize: 18.0, color: Colors.white),),
                           CircleAvatar(
                            radius: 13.5,
                            child: Text('+',style:  TextStyle( fontSize: 22.0),),
                            backgroundColor: Colors.white, 
                                  )
                                ],
                              ),
                             ),
                            )
                        ],
                      ),
                    ),
                    Container(
                    width: 1.0,
                //color: Colors.amber,
                margin: const EdgeInsets.all(7.0),
                
                decoration:  BoxDecoration(
                   color: Colors.white,  
                   borderRadius: BorderRadius.circular(13.0),
                   boxShadow: const [
                      BoxShadow(
                          color: Colors.black38,
                          offset: Offset(0.0, 1.5),
                          blurStyle: BlurStyle.outer,
                          blurRadius: 2.5)
                    ]),
                            
                padding: const EdgeInsets.only(bottom: 10.0),
                child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                 children: [
                    ClipRRect(
                      borderRadius: const BorderRadius.only(topLeft: Radius.circular(13.0), topRight: Radius.circular(13.0)),
                      child: Container(
                          width: MediaQuery.of(context).size.width,
                          height: 100,
                          decoration: const BoxDecoration(
                            image: DecorationImage(
                              fit: BoxFit.fill,
                              image: AssetImage("assets/image/product_2.jpeg"),
                            ),
                          ),
                        ),
                    ),
                  
                  
                    // ClipRRect(
                    //   borderRadius: const BorderRadius.only(topLeft: Radius.circular(13.0), topRight: Radius.circular(13.0)),
                    //   child: Container(
                    //     width: 182.0,
                    //     color: Colors.red,
                    //     child: Image.asset('assets/image/product_1.jpeg'),
                    //   ),
                    // ),
                 
                  const SizedBox(height: 10.0),
                
                  Container(
                    padding: const EdgeInsets.only(left: 8.0),
                    
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: const [
                         Text('Grabanzos \nHorneados Ajo y \nCebolla - Gozana ', 
                         style:  TextStyle( fontSize: 18.0, 
                         fontWeight: FontWeight.bold, color: Color(0xff00003D) ),
                         ),
                         SizedBox(height: 5.0),
                         Text('90gr', style: TextStyle(color: Colors.grey), 
                         ),
                         SizedBox(height: 5.0),
                         Text('Gozana Snacks', style: TextStyle(color: Colors.green, fontWeight: FontWeight.bold), ), 
                         SizedBox(height: 18.0),
                      ],
                    ),
                  ),
                  

                Row(
                  children: const [
                    SizedBox(width: 17),
                     Text('S/ 11.00', style: TextStyle(color: Colors.black, fontSize: 20.0, fontWeight: FontWeight.bold), ),
                      SizedBox(width: 10.0),
                     //Text('S/ 19.00', style: TextStyle(color: Colors.grey, fontSize: 14.0, fontWeight: FontWeight.bold), ),                   
                  ],
                ), 
                
                
                  const Spacer(),
                  Center(
                    child: Container(
                     decoration: BoxDecoration(
                      color: Colors.green,
                    border: Border.all(color: Colors.grey),
                    borderRadius: BorderRadius.circular(25)

                     ),
                        width: 150.0,
                        height: 35.0,
                      
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                       
                        children: const [
                          
                           //SizedBox(),
                           CircleAvatar(
                            radius: 13.5,
                            child:  Text('—', style:  TextStyle(fontSize: 25.0),),
                            backgroundColor: Colors.white, 
                          ),
                          //SizedBox(width: 15,),
                            Text('1', style:  TextStyle(fontSize: 18.0, color: Colors.white),),
                           CircleAvatar(
                            radius: 13.5,
                            child: Text('+',style:  TextStyle( fontSize: 22.0),),
                            backgroundColor: Colors.white, 
                                  )
                                ],
                              ),
                             ),
                  )
                        ],
                      ),
                    ),
                  
                    Container(
                    width: 1.0,
                //color: Colors.amber,
                margin: const EdgeInsets.all(7.0),
                
                decoration:  BoxDecoration(
                   color: Colors.white,  
                   borderRadius: BorderRadius.circular(13.0),
                   boxShadow: const [
                      BoxShadow(
                          color: Colors.black38,
                          offset: Offset(0.0, 1.5),
                          blurStyle: BlurStyle.outer,
                          blurRadius: 2.5)
                    ]),
                            
                padding: const EdgeInsets.only(bottom: 10.0),
                child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                 children: [
                    ClipRRect(
                      borderRadius: const BorderRadius.only(topLeft: Radius.circular(13.0), topRight: Radius.circular(13.0)),
                      child: Container(
                          width: MediaQuery.of(context).size.width,
                          height: 100,
                          decoration: const BoxDecoration(
                            image: DecorationImage(
                              fit: BoxFit.fill,
                              image: AssetImage("assets/image/product_3.jpeg"),
                            ),
                          ),
                        ),
                    ),
                  
                  
                    // ClipRRect(
                    //   borderRadius: const BorderRadius.only(topLeft: Radius.circular(13.0), topRight: Radius.circular(13.0)),
                    //   child: Container(
                    //     width: 182.0,
                    //     color: Colors.red,
                    //     child: Image.asset('assets/image/product_1.jpeg'),
                    //   ),
                    // ),
                 
                  const SizedBox(height: 10.0),
                
                  Container(
                    padding: const EdgeInsets.only(left: 8.0),
                    
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: const [
                         Text('Almendra Cubierta \nen Chocolate -\nGozana ', 
                         style:  TextStyle( fontSize: 18.0, 
                         fontWeight: FontWeight.bold, color: Color(0xff00003D) ),
                         ),
                         SizedBox(height: 5.0),
                         Text('12gr', style: TextStyle(color: Colors.grey), 
                         ),
                         SizedBox(height: 5.0),
                         Text('Gozana', style: TextStyle(color: Colors.green, fontWeight: FontWeight.bold), ), 
                         SizedBox(height: 18.0),
                      ],
                    ),
                  ),
                  

                Row(
                  children: const [
                    SizedBox(width: 17),
                     Text('S/ 15.00', style: TextStyle(color: Colors.black, fontSize: 20.0, fontWeight: FontWeight.bold), ),
                      SizedBox(width: 10.0),
                     Text('S/ 19.00', style: TextStyle(color: Colors.grey, fontSize: 14.0, fontWeight: FontWeight.bold), ),                   
                  ],
                ), 
                
                
                  const Spacer(),
                  Center(
                    child: Container(
                     decoration: BoxDecoration(
                      color: Colors.green,
                    border: Border.all(color: Colors.grey),
                    borderRadius: BorderRadius.circular(25)

                     ),
                        width: 150.0,
                        height: 35.0,
                      
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                       
                        children: const [
                          
                           //SizedBox(),
                           CircleAvatar(
                            radius: 13.5,
                            child:  Text('—', style:  TextStyle(fontSize: 25.0),),
                            backgroundColor: Colors.white, 
                          ),
                          //SizedBox(width: 15,),
                            Text('1', style:  TextStyle(fontSize: 18.0, color: Colors.white),),
                           CircleAvatar(
                            radius: 13.5,
                            child: Text('+',style:  TextStyle( fontSize: 22.0),),
                            backgroundColor: Colors.white, 
                                  )
                                ],
                              ),
                             ),
                  )
                        ],
                      ),
                    ),
                  
                    Container(
                    width: 1.0,
                //color: Colors.amber,
                margin: const EdgeInsets.all(7.0),
                
                decoration:  BoxDecoration(
                   color: Colors.white,  
                   borderRadius: BorderRadius.circular(13.0),
                   boxShadow: const [
                      BoxShadow(
                          color: Colors.black38,
                          offset: Offset(0.0, 1.5),
                          blurStyle: BlurStyle.outer,
                          blurRadius: 2.5)
                    ]),
                            
                padding: const EdgeInsets.only(bottom: 10.0),
                child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                 children: [
                    ClipRRect(
                      borderRadius: const BorderRadius.only(topLeft: Radius.circular(13.0), topRight: Radius.circular(13.0)),
                      child: Container(
                          width: MediaQuery.of(context).size.width,
                          height: 100,
                          decoration: const BoxDecoration(
                            image: DecorationImage(
                              fit: BoxFit.fill,
                              image: AssetImage("assets/image/product_4.jpeg"),
                            ),
                          ),
                        ),
                    ),
                  
                  
                    // ClipRRect(
                    //   borderRadius: const BorderRadius.only(topLeft: Radius.circular(13.0), topRight: Radius.circular(13.0)),
                    //   child: Container(
                    //     width: 182.0,
                    //     color: Colors.red,
                    //     child: Image.asset('assets/image/product_1.jpeg'),
                    //   ),
                    // ),
                 
                  const SizedBox(height: 10.0),
                
                  Container(
                    padding: const EdgeInsets.only(left: 8.0),
                    
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: const [
                         Text('Almendra Cubierta \nen Chocolate -\nGozana ', 
                         style:  TextStyle( fontSize: 18.0, 
                         fontWeight: FontWeight.bold, color: Color(0xff00003D) ),
                         ),
                         SizedBox(height: 5.0),
                         Text('12gr', style: TextStyle(color: Colors.grey), 
                         ),
                         SizedBox(height: 5.0),
                         Text('Gozana', style: TextStyle(color: Colors.green, fontWeight: FontWeight.bold), ), 
                         SizedBox(height: 18.0),
                      ],
                    ),
                  ),
                  

                Row(
                  children: const [
                    SizedBox(width: 17),
                     Text('S/ 15.00', style: TextStyle(color: Colors.black, fontSize: 20.0, fontWeight: FontWeight.bold), ),
                      SizedBox(width: 10.0),
                     Text('S/ 19.00', style: TextStyle(color: Colors.grey, fontSize: 14.0, fontWeight: FontWeight.bold), ),                   
                  ],
                ), 
                
                
                  const Spacer(),
                  Center(
                    child: Container(
                     decoration: BoxDecoration(
                      color: Colors.green,
                    border: Border.all(color: Colors.grey),
                    borderRadius: BorderRadius.circular(25)

                     ),
                        width: 150.0,
                        height: 35.0,
                      
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                       
                        children: const [
                          
                           //SizedBox(),
                           CircleAvatar(
                            radius: 13.5,
                            child:  Text('—', style:  TextStyle(fontSize: 25.0),),
                            backgroundColor: Colors.white, 
                          ),
                          //SizedBox(width: 15,),
                            Text('1', style:  TextStyle(fontSize: 18.0, color: Colors.white),),
                           CircleAvatar(
                            radius: 13.5,
                            child: Text('+',style:  TextStyle( fontSize: 22.0),),
                            backgroundColor: Colors.white, 
                                  )
                                ],
                              ),
                             ),
                  )
                        ],
                      ),
                    ),
                                    ]
                ),
                     ]
                    ),
                  )
    );
  }
}