import 'package:flutter/material.dart';
import 'package:screens_de_pruebas/grid_productos.dart';


void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Material App',
      home: Scaffold(
         
        body: GridProductos()
        ),
      );
    
  }
}